# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only

import json

from ansible.module_utils.basic import AnsibleModule


class ProxmoxShell(AnsibleModule):
    def run(self, get_path: str, create_path: str = None, modify_path: str = None):
        """
        Prepare the pvesh app; post __init__ function.

        Parameters
        ----------
        path: str
            API path
        """
        self.warnings = []
        self.changed = False
        self.data = {}
        self.get_path = get_path
        self.create_path = create_path or get_path
        self.modify_path = modify_path or get_path
        self.load()

        self.on_detect_action()

        if self.changed:
            self.load()

    @property
    def state(self):
        return self.data["state"]

    def on_detect_action(self):
        if self.params["state"] == "present" and self.state == "absent":
            self.do_create_cmdline()
            self.changed = True
        elif self.params["state"] == "present" and self.state == "present":
            self.changed |= self.do_modify_cmdline()
        elif self.params["state"] == "absent" and self.state == "absent":
            return
        elif self.params["state"] == "absent" and self.state == "present":
            self.do_delete_cmdline()
            self.changed = True

    def on_load(self):
        """
        Virtual function
        """
        return

    def _on_load(self):
        """
        Virtual function
        """
        self.on_load()
        self.data["state"] = "present"

    def load(self):
        """
        Execute `item_path` as a query to get the information about the item
        """
        cmd = self.start_new_cmdline("get", self.get_path)
        _retval, stdout, _stderr = self.execute_command(cmd, ignore_fail=True)

        # TODO: if retval
        try:
            self.data = json.loads(stdout)
            self._on_load()
            return
        except (json.decoder.JSONDecodeError, LookupError):
            self.data = {"state": "absent"}

    def execute_command(self, cmd, ignore_fail=False):
        if cmd is None:
            self.fail_json(msg="on_create_cmdline / on_modify_cmdline must return command 'cmd'", rc=1)
        elif isinstance(cmd, list):
            cmd = [str(x) for x in cmd]
        else:
            cmd = cmd.split()

        # self.warnings += [" ".join(cmd)]
        retval, stdout, stderr = self.run_command(cmd)
        if retval and not ignore_fail:
            self.fail_json(msg=cmd, rc=retval, stdout=stdout, stderr=stderr)
        return (retval, stdout, stderr)

    def start_new_cmdline(self, method, path):
        cmd = ["pvesh", method, path]
        if method == "get":
            cmd.append("--output-format=json")
        return cmd

    def do_create_cmdline(self):
        cmd = self.start_new_cmdline("create", self.create_path)
        cmd = self.on_create_cmdline(cmd)
        self.execute_command(cmd)

    def on_create_cmdline(self, cmd):
        """
        Virtual function
        """
        return self.on_modify_cmdline(cmd)

    def do_modify_cmdline(self):
        cmd = self.start_new_cmdline("set", self.modify_path)
        cmd = self.on_modify_cmdline(cmd)
        # Command line is not modified
        if cmd is None:
            return False
        if len(cmd) <= 3:
            return False
        self.execute_command(cmd)
        return True

    def on_modify_cmdline(self, cmd):
        """
        Virtual function
        """
        return cmd

    def do_delete_cmdline(self):
        cmd = self.start_new_cmdline("delete", self.modify_path)
        cmd = self.on_delete_cmdline(cmd)
        self.execute_command(cmd)

    def on_delete_cmdline(self, cmd):
        """
        Virtual function
        """
        return cmd

    def parameter_changed(self, parameter_key, data_key=None):
        r"""
        Check that param has been set,
        and t\hat it is different to what already exists
        """
        value = self.params.get(parameter_key)
        if value is None:
            return False

        # key in self.data is *usually* the same as self.params
        data_key = data_key or parameter_key
        return value != self.data.get(data_key)

    def maybe_command(self, parameter_key, command_key=None, data_key=None):
        if not self.parameter_changed(parameter_key, data_key):
            return []

        value = self.params[parameter_key]
        if value in [False, True]:
            value = "1" if value else "0"

        command_key = command_key or parameter_key
        return [f"-{command_key}", value or ""]
