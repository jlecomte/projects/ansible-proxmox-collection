#!/usr/bin/env python3
# Copyright (C) 2022 Julien Lecomte <julien@lecomte.at>
# SPDX-License-Identifier: GPL-3.0-only


from ansible_collections.julien_lecomte.proxmox.plugins.module_utils import misc
from ansible_collections.julien_lecomte.proxmox.plugins.module_utils.pvesh import ProxmoxShell

DOCUMENTATION = r"""
---
module: group
plugin_type: module
author: Julien Lecomte (julien@lecomte.at)
short_description: Adds, modifies, or removes a Proxmox group.
description:
  - Adds, modifies, or removes a Proxmox group.
  - Returned values will exist in a variable named 'group'.
seealso:
  - name: PVE API
    description: Proxmox VE Application Programming Interface
    link: https://pve.proxmox.com/pve-docs/api-viewer/#/access/groups
options:
  name:
    description:
      - The group name.
    type: str
    required: true
  state:
    description:
      - Specify if the group should exist (present) or absent.
    type: str
    choices: [ absent, present ]
    default: present
  comment:
    description:
      - Optionally sets the comment field.
    type: str
"""

EXAMPLES = r"""
- name: Create group 'example'
  julien_lecomte.proxmox.group:
    name: 'example'

- name: Create group 'example' with a comment
  julien_lecomte.proxmox.group:
    name: 'example'
    comment: 'Lorem ipsum'
"""

RETURN = r"""
name:
  description: Proxmox group name.
  returned: always
  type: str
state:
  description: State ("absent" or "present").
  returned: always
  type: str
comment:
  description: Comment field.
  returned: when group exists
  type: str
users:
  description: Group users.
  returned: when group exists
  type: list
  elements: str
"""


class ProxmoxGroup(ProxmoxShell):
    def on_load(self):
        self.data["users"] = misc.split_into_list(self.data.pop("users", None))

    def on_create_cmdline(self, cmd):
        cmd.extend(["-groupid", self.params["name"]])
        return self.on_modify_cmdline(cmd)

    def on_modify_cmdline(self, cmd):
        cmd.extend(self.maybe_command("comment"))
        return cmd


def main():
    module = ProxmoxGroup(
        argument_spec=dict(
            name=dict(type="str", required=True),
            state=dict(type="str", default="present", choices=["absent", "present"]),
            comment=dict(type="str"),
        ),
        supports_check_mode=False,
    )
    module.params["groupid"] = module.params["name"]
    module.run(f"/access/groups/{module.params['name']}", "/access/groups")
    module.exit_json(changed=module.changed, warnings=module.warnings, group=module.data)


if __name__ == "__main__":
    main()
