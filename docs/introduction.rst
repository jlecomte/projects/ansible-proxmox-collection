Introduction
============

This Ansible collection contains a Proxmox VE playbook, a role and module plugins.

The playbook and role are DebOps compatible and are written in the DebOps style. They can be used with or without DebOps.

The module plugins, used by the role, do not require an Proxmox API key. They directly use the shell commands available on a Proxmox such as :command:`pveum` (user management command).

.. seealso::

  `Proxmox VE <https://www.proxmox.com/en/proxmox-ve>`__
    Proxmox Virtual Environment documentation.

  `DebOps <https://docs.debops.org/en/latest/>`__
    The DebOps project is a set of Free and Open Source tools that let users bootstrap and manage an IT infrastructure based on Debian or Ubuntu operating systems.
