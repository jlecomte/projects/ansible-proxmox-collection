
.. clusters:

Proxmox Clusters
================

Proxmox hosts can be clustered together and will automatically sync the configuration.

Because of this, a task that is to run on several hosts at the same time can fail on at least one of the other hosts of the cluster.

It is recommended to enable the Proxmox tasks only for one host per cluster.

If using the Debops playbook and role, you can set the variable *proxmox__enabled* to False for all hosts but one per cluster.
