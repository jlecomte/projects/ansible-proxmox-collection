
Playbook
========

Usage
-----

A Proxmox playbook named :file:`proxmox` will be installed with this collection.

It can be used directly from command line, with either :command:`ansible-playbook` or :command:`debops run` commands:

.. code-block:: bash

    # if using Ansible
    ansible-playbook julien_lecomte.proxmox.proxmox

.. code-block:: bash

    # if using DebOps
    debops run julien_lecomte.proxmox.proxmox

The Proxmox playbook may also be used with a simple Ansible `import_playbook` keyword within an already existing playbook.

.. code-block:: yaml

    ---
    - import_playbook: julien_lecomte.proxmox.proxmox

If you do not wish to use the provided playbook, you can use the Ansible *role* keyword to include the role in one of your pre-existing playbooks:

.. code-block:: yaml

    ---
    - name: Manage Proxmox
      hosts: all
      become: True

      roles:
        - role: julien_lecomte.proxmox.proxmox


Sample DebOps site.yml
^^^^^^^^^^^^^^^^^^^^^^

If you wish to run the :file:`proxmox` playbook automatically after the configuration of a host, you can create your own :file:`site.yml` in your project. This file would first run the DebOps ``bootstraping`` playboook, then the DebOps ``site`` playbook, and last but not least, the ``proxmox`` playbook.

.. code-block:: yaml

   ---

   - import_playbook: debops.debops.bootstrap
   - import_playbook: debops.debops.site
   - import_playbook: julien_lecomte.proxmox.proxmox


Inventory
---------

In order for hosts to be configured by the included Proxmox playbook, the host has to be in the :file:`debops_service_proxmox` group, and the :file:`proxmox__enabled` variable has to be set to `True`.


Sample Ansible yaml inventory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: yaml

   ---
   all:
     children:
       debops_service_proxmox:
         hosts:
           server.example.com:
             proxmox__enabled: True


Sample Roster yaml inventory
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is an example minimal inventory in the  `Ansible Roster inventory`__ format.

.. __: https://gitlab.com/jlecomte/projects/ansible-roster

.. code-block:: yaml

   ---
   plugin: roster

   hosts:
     server.example.com:
       groups:
         - debops_service_proxmox
       vars:
         proxmox__enabled: True


.. seealso::

  `Ansible playbook command <https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html>`__
    `ansible-playbook`, the tool to run Ansible playbooks.

  `DebOps run command <https://docs.debops.org/en/master/user-guide/scripts/debops-run/index.html>`__
    `debops run` command, the tool to run DebOps playbooks.

  `ansible.builtin.import_playbook <https://docs.ansible.com/ansible/latest/collections/ansible/builtin/import_playbook_module.html>`__
    `ansible.builtin.import_playbook` keyword documentation.

  `Ansible Roster inventory plugin <https://gitlab.com/jlecomte/projects/ansible-roster>`__
    Roster is an Ansible inventory plugin with focus on groups applied to hosts instead of hosts included in groups.
