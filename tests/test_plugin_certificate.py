from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import certificate


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
      ( #-----------------------------------------------------------------------
        # absent to absent
        {"state": "absent", "node": "example"},
        [
            ("pvesh get /nodes/example/certificates/info --output-format=json", None),
        ],
        { "changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # present to absent
        {"state": "absent", "node": "example"},
        [
            ("pvesh get /nodes/example/certificates/info --output-format=json", [{"filename":"pveproxy-ssl.pem"}, {"filename":"one"}, {"filename":"two"}]),
            ("pvesh delete /nodes/example/certificates/custom -restart 1", None),
            ("pvesh get /nodes/example/certificates/info --output-format=json", None),
        ],
        { "changed": True},
      ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_group(plugin, params, commands, output):
    plugin.run(params, commands, output, certificate.main)
