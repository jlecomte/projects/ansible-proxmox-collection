from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import user


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
      ( #-----------------------------------------------------------------------
        # absent to absent
        {"name": "example@pam", "state": "absent"},
        [
            ("pvesh get /access/users/example@pam --output-format=json", None),
        ],
        { "changed": False, "warnings": []},
      ),
      ( #-----------------------------------------------------------------------
        # absent to present
        {"name": "example@pam", "state": "present"},
        [
            ("pvesh get /access/users/example@pam --output-format=json", None),
            ("pvesh create /access/users -userid example@pam", {}),
            ("pvesh get /access/users/example@pam --output-format=json", {"userid": "example@pam"}),
        ],
        { "changed": True, "warnings": []},
      ),
      ( #-----------------------------------------------------------------------
        # present to present
        {"name": "example@pam", "state": "present"},
        [
            ("pvesh get /access/users/example@pam --output-format=json", {"userid": "example@pam"}),
        ],
        { "changed": False, "warnings": []},
      ),
      ( #-----------------------------------------------------------------------
        # present to absent
        {"name": "example@pam", "state": "absent"},
        [
            ("pvesh get /access/users/example@pam --output-format=json", {"userid": "example@pam"}),
            ("pvesh delete /access/users/example@pam", {}),
            ("pvesh get /access/users/example@pam --output-format=json", None),
        ],
        { "changed": True, "warnings": []},
      ),
      ( #-----------------------------------------------------------------------
        # present to absent
        {"name": "example@pam", "state": "present", "expire":-66},
        [
            ("pvesh get /access/users/example@pam --output-format=json", None),
        ],
        { "msg": "expire must be 0 or a positive integer"},
      ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_user(plugin, params, commands, output):
    plugin.run(params, commands, output, user.main)
