import os
import shutil
import subprocess

from pytest import fixture, mark

TESTS_PATH = "ansible_collections/julien_lecomte/proxmox/tests"
TARGETS_PATH = "integration/targets"


@fixture
def change_exec_dir(monkeypatch):
    monkeypatch.chdir(TESTS_PATH)


@mark.order(3)
@mark.skipif(
    not os.getenv("ANSIBLE_TEST_FLAGS"),
    reason="Environment variable ANSIBLE_TEST_FLAGS is not set. Please read CONTRIBUTING.md",
)
@mark.parametrize("path", os.listdir(f"{TESTS_PATH}/{TARGETS_PATH}"))
def test_roster_plugin_success(change_exec_dir, path):  # pylint: disable=unused-argument,redefined-outer-name
    flags = os.getenv("ANSIBLE_TEST_FLAGS")
    flags = flags.replace("'", "")
    flags = flags.replace('"', "")

    cmd = os.path.abspath(shutil.which("ansible-test")) + f" integration {flags} {path}"
    retval = subprocess.run(cmd.split(), shell=False, capture_output=True, text=True, env=os.environ, check=False)
    retval.check_returncode()
