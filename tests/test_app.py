# from pytest import fixture, mark

# TODO: Enable unit tests
#
# @mark.parametrize(
#    "params, param_key, data, data_key, expected",
#    [
#        # fmt: off
#        # no parameters passed:
#        ({}, "comment", {}, None, False),
#        ({}, "comment", {"comment": ""}, None, False),
#        ({}, "comment", {"comment": "value"}, None, False),
#        # parameter name mismatch
#        ({"other": "value"}, "comment", {}, None, False),
#        ({"other": "value"}, "comment", {"comment": "value"}, None, False),
#        # regular cases:
#        ({"comment": "value"}, "comment", {}, None, True),
#        ({"comment": "value"}, "comment", {"comment": ""}, None, True),
#        ({"comment": "value"}, "comment", {"comment": "other"}, None, True),
#        ({"comment": "same"}, "comment", {"comment": "same"}, None, False),
#        # with an asset key:
#        ({"comment": "value"}, "comment", {"use_param": "value"}, "use_param", False),
#        ({"comment": "other"}, "comment", {"use_param": "value"}, "use_param", True),
#        # fmt: on
#    ],
# )
# @mark.order(1)
# def test_param_changed(module, params, param_key, data, data_key, expected):
#    module.params = params
#    module.data = data
#
#    retval = module.param_changed(param_key, data_key)
#    assert retval == expected
#
#
# @mark.parametrize(
#    "params, param_key, data, data_key, command_key, expected",
#    [
#        # fmt: off
#        # no parameters passed:
#        ({}, "comment", {}, None, None, []),
#        # regular cases:
#        ({"comment": True}, "comment", {}, None, None, ['--comment', '1']),
#        ({"comment": False}, "comment", {}, None, None, ['--comment', '0']),
#        ({"comment": "value"}, "comment", {"comment": ""}, None, None, ['--comment', 'value']),
#        ({"comment": "value"}, "comment", {"comment": "value"}, None, None, []),
#        ({"comment": "value"}, "comment", {"comment": "other"}, None, None, ['--comment', 'value']),
#        ({"comment": ""}, "comment", {"comment": "value"}, None, None, ['--comment', '']),
#        ({}, "comment", {"comment": ""}, None, None, []),
#        ({}, "comment", {"comment": "value"}, None, None, []),
#        ({}, "comment", {"comment": "other"}, None, None, []),
#        # fmt: on
#    ],
# )
# @mark.order(1)
# def test_maybe_command_append(module, params, param_key, data, data_key, command_key, expected):
#    module.params = params
#    module.data = data
#
#    retval = module.maybe_command(param_key, command_key, data_key)
#    assert retval == expected
