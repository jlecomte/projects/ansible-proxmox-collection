from pytest import mark

from ansible_collections.julien_lecomte.proxmox.plugins.modules import group


@mark.parametrize(
    "params, commands, output",
    # fmt: off
    [
      ( #-----------------------------------------------------------------------
        # absent to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /access/groups/example --output-format=json", None),
        ],
        { "changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # absent to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /access/groups/example --output-format=json", None),
            ("pvesh create /access/groups -groupid example", {}),
            ("pvesh get /access/groups/example --output-format=json", {"groupid": "example"}),
        ],
        { "changed": True},
      ),
      ( #-----------------------------------------------------------------------
        # present to present
        {"name": "example", "state": "present"},
        [
            ("pvesh get /access/groups/example --output-format=json", {"groupid": "example"}),
        ],
        { "changed": False},
      ),
      ( #-----------------------------------------------------------------------
        # present to absent
        {"name": "example", "state": "absent"},
        [
            ("pvesh get /access/groups/example --output-format=json", {"groupid": "example"}),
            ("pvesh delete /access/groups/example", {}),
            ("pvesh get /access/groups/example --output-format=json", None),
        ],
        { "changed": True},
      ),
    ],
    # fmt: on
)
@mark.order(2)
def test_plugin_group(plugin, params, commands, output):
    plugin.run(params, commands, output, group.main)
